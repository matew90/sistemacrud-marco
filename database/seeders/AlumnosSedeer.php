<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class AlumnosSedeer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

     public function run()
    {
        DB::table('alumnos')->insert([
            'nombre' => 'paco',
            'apellido_paterno' => 'contreras',
            'apellido_materno' => 'rodriguez',
            'semestre' => 2,
            'grupo' => 'A',
        ]);       //
    }
}
