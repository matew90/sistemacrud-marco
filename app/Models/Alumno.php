<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\models\alumno;

class Alumno extends Model
{
    use HasFactory;
    protected $table='alumnos';
}
